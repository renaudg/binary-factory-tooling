// Request a node to be allocated to us
node( "AndroidSDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// Make sure the Android SDK is setup properly
		stage('Setting up SDK') {
			// For this we need to ensure that the Android 24 or later SDK is installed, otherwise the APK generation will fail
			sh """
				sdkmanager "platforms;android-28"
			"""
		}

		// Now retrieve the artifacts
		stage('Retrieving App Bundle Artifacts') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-appbundle-artifacts-arm64-v8a.tar', projectName: 'Krita_Release_Android_arm64-v8a_Build'
			copyArtifacts filter: 'krita-appbundle-artifacts-armeabi-v7a.tar', projectName: 'Krita_Release_Android_armeabi-v7a_Build'
			copyArtifacts filter: 'krita-appbundle-artifacts-x86_64.tar', projectName: 'Krita_Release_Android_x86_64_Build'
			copyArtifacts filter: 'krita-appbundle-artifacts-x86.tar', projectName: 'Krita_Release_Android_x86_Build'
			copyArtifacts filter: 'krita-android-deps.tar', projectName: 'Krita_Android_arm64-v8a_Dependency_Build'

			// Now we unpack them
			sh """
				tar -xf $WORKSPACE/krita-appbundle-artifacts-arm64-v8a.tar
				tar -xf $WORKSPACE/krita-appbundle-artifacts-armeabi-v7a.tar
				tar -xf $WORKSPACE/krita-appbundle-artifacts-x86.tar
				tar -xf $WORKSPACE/krita-appbundle-artifacts-x86_64.tar
				tar -xf $WORKSPACE/krita-android-deps.tar
			"""
		}

		stage('Building App Bundle') {
			sh """
				export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
				export KRITA_BUILD_APPBUNDLE=1
				export APK_PATH=$WORKSPACE/build/krita_build_apk/

				cd \$APK_PATH
				\$APK_PATH/gradlew bundleRelease

				chmod 644 \$APK_PATH/build/outputs/bundle/release/*.aab
				mv \$APK_PATH/build/outputs/bundle/release/*.aab $WORKSPACE
			"""

			archiveArtifacts artifacts: '*.aab', onlyIfSuccessful: true
		}
	}
}
}
